const { File, Text } = require('@keystonejs/fields');
const { LocalFileAdapter } = require('@keystonejs/file-adapters');
const {
  AuthedRelationship,
} = require('@keystonejs/fields-authed-relationship');

module.exports = {
  fields: {
    title: {
      type: Text,
      isRequired: true,
    },
    description: { type: Text },
    author: {
      type: AuthedRelationship,
      ref: 'User',
      isRequired: true,
    },
    file: {
      type: File,
      adapter: new LocalFileAdapter({
        src: './app/public/files',
        path: '/files',
      }),
      isRequired: true,
    },
  },
  labelResolver: (item) => item.title,
};
