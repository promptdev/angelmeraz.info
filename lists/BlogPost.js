const { DateTime, Integer, Text, Select, Slug } = require('@keystonejs/fields');
const {
  AuthedRelationship,
} = require('@keystonejs/fields-authed-relationship');

module.exports = {
  fields: {
    title: {
      type: Text,
      isRequired: true,
    },
    slug: {
      type: Slug,
      from: 'title',
      isUnique: true,
      isRequired: true,
    },
    order: {
      type: Integer,
    },
    status: {
      type: Select,
      defaultValue: 'draft',
      options: [
        { label: 'Draft', value: 'draft' },
        { label: 'Published', value: 'published' },
      ],
    },
    body: {
      adminDoc: 'Accepts Markdown and HTML',
      type: Text,
      isMultiline: true,
    },
    author: {
      type: AuthedRelationship,
      ref: 'User',
      isRequired: true,
    },
    publishedDate: {
      type: DateTime,
      defaultValue: new Date().toISOString(),
    },
  },
  labelResolver: (item) => item.title,
};
