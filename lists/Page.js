const { Checkbox, Integer, Text, Select, Slug } = require('@keystonejs/fields');
const {
  AuthedRelationship,
} = require('@keystonejs/fields-authed-relationship');

module.exports = {
  fields: {
    title: {
      type: Text,
      isRequired: true,
    },
    slug: {
      type: Slug,
      from: 'title',
      isUnique: true,
      isRequired: true,
    },
    colorScheme: {
      type: Select,
      defaultValue: 'default',
      options: [
        { label: 'Default', value: 'default' },
        { label: 'Dark', value: 'dark' },
        { label: 'Blue', value: 'blue' },
        { label: 'Orange', value: 'orange' },
      ],
    },
    isMenuItem: {
      type: Checkbox,
      label: 'Display in menu',
    },
    order: {
      type: Integer,
    },
    status: {
      type: Select,
      defaultValue: 'draft',
      options: [
        { label: 'Draft', value: 'draft' },
        { label: 'Published', value: 'published' },
      ],
    },
    layout: {
      type: Select,
      defaultValue: 'global',
      options: 'global, projects',
    },
    body: {
      adminDoc: 'Accepts Markdown and HTML',
      type: Text,
      isMultiline: true,
    },
    author: {
      type: AuthedRelationship,
      ref: 'User',
      isRequired: true,
    },
  },
  labelResolver: (item) => item.title,
};
