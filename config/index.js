require('dotenv').config();

const PROJECT_NAME = 'angelmeraz-info';

module.exports = {
  DATABASE_URI: process.env.MONGO_URI,
  PROJECT_NAME,
};
