/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './app/components/**/*.{js,jsx,ts,tsx}',
    './app/pages/**/*.{js,jsx,ts,tsx}',
    './app/cms-md/*.md',
  ],
  theme: {
    extend: {
      animation: {
        blink: 'blink 0.6s steps(2, start) infinite',
      },
      colors: {
        accent: 'var(--color-accent)',
        background: 'var(--color-background)',
        text: 'var(--color-text)',
      },
      fontFamily: {
        sans: [
          '"Simple"',
          'ui-monospace',
          'SFMono-Regular',
          'Menlo',
          'Monaco',
          'Consolas',
          '"Liberation Mono"',
          '"Courier New"',
          'monospace',
        ],
      },
      keyframes: {
        blink: {
          to: { visibility: 'hidden' },
        },
      },
      typography: {
        DEFAULT: {
          css: {
            a: {
              color: 'var(--color-accent)',
              textDecoration: '',
              fontWeight: '100',
            },
            strong: {
              color: 'var(--color-accent)',
              fontWeight: '600',
            },
          },
        },
        lg: {
          css: {
            lineHeight: '1.75rem',
            p: {
              marginBottom: '1.75rem',
            },
          },
        },
      },
    },
  },
  plugins: [require('@tailwindcss/typography')],
};
