module.exports = {
  DOMAIN: process.env.DOMAIN,
  SLUGS: {
    HOMEPAGE: 'start',
    PROJECTS_INDEX: 'work',
    BLOG_INDEX: 'blog',
  },
};
