import Markdown from './Markdown';

export default function Projects({ data }) {
  return (
    <>
      {data.map((project, index) => {
        return (
          <div key={index}>
            <div className="project--wrap grid">
              <div className="project grid__cell 1/4 1/1--mobile 3/10--tablet">
                <Markdown input={project.content} />
              </div>
              <div className="project__gallery grid__cell 3/4 1/1--mobile 7/10--tablet">
                <div className="gallery">
                  <Markdown input={project.gallery} />
                </div>
              </div>
            </div>
          </div>
        );
      })}
    </>
  );
}
