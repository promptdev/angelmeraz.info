import MarkdownToJSX from 'markdown-to-jsx';

export default function Markdown({ input }) {
  if (!input) return null;

  return <MarkdownToJSX>{input}</MarkdownToJSX>;
}
