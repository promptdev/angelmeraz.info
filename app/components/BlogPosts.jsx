import Markdown from './Markdown';

export default function BlogPosts({ data }) {
  return (
    <>
      {data.map((post, index) => {
        return (
          <div key={index}>
            <Markdown input={post.body} />
          </div>
        );
      })}
    </>
  );
}
