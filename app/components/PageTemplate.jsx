import { useEffect, useState } from 'react';
import ClassGroup from 'classgroup/commonjs';

// import BlogPosts from './BlogPosts';
import Footer from '../components/Footer';
import Markdown from './Markdown';
// import Projects from './Projects';

function ThemeButtonIcon({ toggle }) {
  if (toggle) {
    return <i className="fa-regular fa-sun" />;
  }

  return <i className="fa-solid fa-moon" />;
}

export default function PageTemplate({
  // colorScheme,
  header,
  nav,
  // layout,
  body,
  // slug,
  // projectsData,
  // blogPostsData,
}) {
  const [isThemeToggled, setThemeToggle] = useState(false);
  const handleThemeToggle = () => setThemeToggle(!isThemeToggled);

  useEffect(() => {
    isThemeToggled && document.body.classList.add('bright');
    return () => isThemeToggled && document.body.classList.remove('bright');
  });

  const tw = ClassGroup({
    main: {
      a: 'text-text text-lg font-thin',
      l: 'p-8 md:p-20',
    },
    header: {},
    themeButton: {
      a: 'text-accent hover:text-text',
      l: 'flex items-center justify-center h-8 w-8',
      p: {
        d: 'absolute top-7 right-6 z-10',
        md: 'md:fixed md:top-[102px] md:right-6',
      },
      t: 'transition-colors',
    },
    container: 'md:flex md:gap-8',
    aside: {
      a: 'text-accent',
      l: 'w-32',
      md: 'md:pt-6',
    },
    content: {
      g: 'prose md:prose-lg text-text',
      li: 'prose-li:marker:text-accent',
      h2: 'prose-h2:text-accent prose-h2:font-thin',
      h3: 'prose-h3:text-accent prose-h3:font-thin',
      h4: 'prose-h4:text-accent prose-h4:font-thin',
      blockquote: {
        a: 'prose-blockquote:border-0 prose-blockquote:text-accent prose-blockquote:font-thin',
        l: 'prose-blockquote:pl-0 prose-blockquote:my-6',
      },
    },
  });

  return (
    <main className={tw.main}>
      {header && <header className={tw.header}>{header}</header>}

      <button className={tw.themeButton} onClick={handleThemeToggle}>
        <ThemeButtonIcon toggle={isThemeToggled} />
      </button>

      <div className={tw.container}>
        {nav && <aside className={tw.aside}>{nav}</aside>}
        {body && (
          <div className={tw.content}>
            <Markdown input={body} />
          </div>
        )}

        {/* {projectsData && <Projects data={projectsData} />} */}
        {/* {blogPostsData && <BlogPosts data={blogPostsData} />} */}
      </div>

      <Footer />
    </main>
  );
}
