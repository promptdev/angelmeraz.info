import ClassGroup from 'classgroup/commonjs';

export default function Footer(y) {
  const tw = ClassGroup({
    footer: {
      d: 'hidden lg:block',
      l: 'fixed bottom-8 right-12',
      a: 'text-text text-xs',
      t: 'origin-bottom-right rotate-90',
    },
    anchor: {
      a: 'text-accent',
    },
  });

  return (
    <footer className={tw.footer}>
      Site powered by{' '}
      <a className={tw.anchor} href="https://heroku.com" target="_blank">
        Heroku
      </a>{' '}
      &amp;{' '}
      <a
        className={tw.anchor}
        href="https://www.mongodb.com/cloud/atlas"
        target="_blank"
      >
        MongoDB Atlas
      </a>
      . Built with{' '}
      <a className={tw.anchor} href="https://nextjs.org/" target="_blank">
        Next.js
      </a>{' '}
      &amp;{' '}
      <a
        className={tw.anchor}
        href="https://www.keystonejs.com/"
        target="_blank"
      >
        KeystoneJS
      </a>
      . | Angel Meraz @ {new Date().getFullYear()}
    </footer>
  );
}
