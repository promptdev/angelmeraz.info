import ClassGroup from 'classgroup/commonjs';
import Document, { Html, Head, Main, NextScript } from 'next/document';

class CustomDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    const tw = ClassGroup({
      body: 'bg-background transition-colors',
    });

    return (
      <Html>
        <Head>
          <meta
            name="description"
            content="Angel Meraz, experienced Web Developer &amp; Web Designer based in London."
          />
          <link rel="preload" href="/fonts/fonts.css" as="style" />
          <link rel="stylesheet" href="/fonts/fonts.css" />
          <script
            src="https://kit.fontawesome.com/7b7ffd5b06.js"
            crossOrigin="anonymous"
            type="text/javascript"
          ></script>
          <script
            async
            src={`https://www.googletagmanager.com/gtag/js?id=${process.env.GA_TRACKING_ID}`}
          ></script>
          <script
            dangerouslySetInnerHTML={{
              __html: `
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
              gtag('config', '${process.env.GA_TRACKING_ID}', {
                page_path: window.location.pathname,
              });
            `,
            }}
          />
        </Head>
        <body className={tw.body}>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default CustomDocument;
