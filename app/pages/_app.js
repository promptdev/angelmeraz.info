import '../styles/global.css';
import NoSSR from '../components/NoSSR';

// This default export is required in a new `pages/_app.js` file.
export default function MyApp({ Component, pageProps }) {
  return (
    <NoSSR>
      <Component {...pageProps} />
    </NoSSR>
  );
}
