import CONFIG from '../../config';
import Nav from '../../components/Nav';
import PageTemplate from '../../components/PageTemplate';
import sortByPageOrder from '../../utils/sortByPageOrder';

export async function getStaticProps() {
  const request = await fetch(`${CONFIG.DOMAIN}/admin/api`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      query: `{
        allPages(where: { isMenuItem: true }) {
          id
          isMenuItem
          order
          slug
          status
          title
       }
      }`,
    }),
  });
  const response = await request.json();

  let navItemsData = [];
  const navItems = response.data?.allPages;
  navItems.sort(sortByPageOrder);
  navItems.forEach((page) => {
    if (!(page.status === 'published')) return;

    page.isMenuItem && navItemsData.push(page);
  });

  return {
    props: {
      navItemsData,
    },
  };
}

export default function Page({ navItemsData }) {
  return (
    <PageTemplate
      body={`<div><p>404 - Page Not Found</p></div>`}
      nav={<Nav navItems={navItemsData} />}
    />
  );
}
