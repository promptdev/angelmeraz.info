import Router from 'next/router';

import CONFIG from '../config';
import Nav from '../components/Nav';
import PageTemplate from '../components/PageTemplate';
import sortByPageOrder from '../utils/sortByPageOrder';

export default function Page({
  navItemsData,
  pageData,
  projectsData,
  blogPostsData,
}) {
  if (Object.keys(pageData).length === 0) {
    typeof window !== 'undefined' && Router.push('/404');
  }

  return (
    <PageTemplate
      {...pageData}
      projectsData={projectsData}
      blogPostsData={blogPostsData}
      nav={<Nav navItems={navItemsData} />}
    />
  );
}

export async function getServerSideProps({ res, query, resolvedUrl }) {
  const allPages = `
    allPages {
    body
    colorScheme
    id
    isMenuItem
    layout
    order
    slug
    status
    title
  }`;

  const isProjectsIndex = query?.slug?.[0] === CONFIG.SLUGS.PROJECTS_INDEX;
  const allProjects = `
  allProjects {
    content
    date
    gallery
    id
    order
    slug
    status
    title
  }`;

  const isBlogIndex = query?.slug?.[0] === CONFIG.SLUGS.BLOG_INDEX;
  const allBlogPosts = `
  allBlogPosts {
    body
    id
    order
    publishedDate
    slug
    status
    title
  }`;

  const gqlQuery = `${allPages} ${isProjectsIndex ? allProjects : ''} ${
    isBlogIndex ? allBlogPosts : ''
  }`;

  const request = await fetch(`${CONFIG.DOMAIN}/admin/api`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      query: `{${gqlQuery}}`,
    }),
  });

  const response = await request.json();

  // exit early: redirect to 404 if no response data
  if (res !== 'undefined' && !response.data) {
    res.writeHead(301, {
      Location: '/404',
    });
    res.end();

    return {};
  }

  let navItemsData = [];
  let pageData = {};

  // Pages Data
  const pages = response.data?.allPages;
  pages.sort(sortByPageOrder);
  pages.forEach((page) => {
    if (!(page.status === 'published')) return;

    // navItemsData
    page.isMenuItem && navItemsData.push(page);

    // pageData
    switch (resolvedUrl) {
      // homepage
      case '/':
        if (page.slug === CONFIG.SLUGS.HOMEPAGE) pageData = { ...page };
        break;

      // all other pages
      default:
        const pageSlug = page?.slug.split('/');
        if (pageSlug.toString() === query.slug.toString())
          pageData = { ...page };
        break;
    }
  });

  // Projects Data
  const projects = response.data?.allProjects;
  projects && projects.sort(sortByPageOrder);
  const projectsData =
    (projects &&
      projects.filter((project) => project.status === 'published')) ||
    [];

  // Blog Posts Data
  const blogPosts = response.data?.allBlogPosts;
  blogPosts && blogPosts.sort(sortByPageOrder);
  const blogPostsData =
    (blogPosts && blogPosts.filter((post) => post.status === 'published')) ||
    [];

  return {
    props: {
      navItemsData,
      pageData,
      projectsData,
      blogPostsData,
    },
  };
}
