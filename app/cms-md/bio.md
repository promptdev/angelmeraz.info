> I’ve always had a genuine passion for technology and design.
>
> Throughout my career I’ve enjoyed working as a Web Developer utilising my experience and interest in design, implementing brands into web applications.
>
> This passion drove my skill-set to grow into a Full Stack Engineer which has helped deliver successful and highly engaging products in the global market.

In reversed chronological order, read below an overview from each one of my enriching work experiences.

## iTech Media — 02/2021 - 09/2023

#### Roles:

_Software Engineer_  
_Tech Lead_

<a href="https://itech.media" target="_blank">iTech Media</a> is a company with a single purpose: help people make smart choices online. By informing and entertaining a global audience with over 150+ products, iTech provides unparalleled insight and guidance in the highly competitive world of iGaming.

While at iTech, I had the chance to further develop my engineering skills. I joined as part of the engineering innovation team in charge of conceiving a platform to help ship products with pace. We crafted a performant platform used to power a number of products using cutting edge technology, such as:

- <a href="https://turborepo.org" target="_blank">Turborepo</a>
- <a href="https://kit.svelte.dev" target="_blank">SvelteKit</a>
- <a href="http://tailwindcss.com" target="_blank">Tailwind</a>
- <a href="https://storybook.js.org" target="_blank">Storybook</a>
- <a href="https://www.chromatic.com" target="_blank">Chromatic</a>
- <a href="https://workers.cloudflare.com" target="_blank">CloudFlare Workers</a>
- <a href="https://aws.amazon.com" target="_blank">AWS:</a> ECS, EFS, S3, Route53
- <a href="https://www.contentful.com" target="_blank">Contentful</a>

_All beautifully written in <a href="http://typescriptlang.org" target="_blank">TypeScript</a>._

As part of my tasks I would be closely involved in:

- Working with the Product and Design teams to create a Design System.
- Defining, refining and estimating tasks.
- Developing, maintaining and continuously releasing our products.
- Coming up with ideas to reach our product goals.
- Outlining an overarching content model for our CMS implementation.
- Onboarding and code reviewing my peers work.
- Participating in weekly discussions with the engineering team to further enhance our ways of working, processes and systems.
- Writing an API to connect internal services.

Contractually, I'm not allowed to talk about the products I worked on, only the ones that are open-source:

During my time at iTech, I was presented with the opportunity, and received support, to create and maintain my first open-source project: <a href="https://www.npmjs.com/package/classgroup" target="_blank">ClassGroup</a>.

<a href="https://www.npmjs.com/package/classgroup" target="_blank">ClassGroup</a>

A utility to help keep your CSS classes in JS consistently and semantically grouped - a forward thinking styling methodology taken from <a href="https://cube.fyi/grouping.html#grouping-order" target="_blank">CUBE CSS</a> - while allowing for a separation of concerns. Find more about it <a href="https://github.com/itech-media/classgroup" target="_blank">here</a>.

While ClassGroup is a fairly simple utility, it has proven invaluable to solve the complex issue of theming and delivering a flexible and reusable component library. I'm very proud of how we managed to use it on scale. It allowed us to simplify the development of our products and provided a substantially better development experience.

## Pulse Innovations Ltd. — 11/2017 - 12/2020

#### Roles:

_Frontend Developer_<br>
_Lead Developer_<br>
_Frontend Architect_

<a href="https://www.pulselive.com" target="_blank">Pulselive</a> is a technology company that delivers digital solutions for the world‘s largest sports federations, leagues and clubs.

Working at Pulselive was a fantastic experience! I had the chance to work in very exciting projects with amazing clients.

When I first joined I mainly worked on the <a href="https://www.premierleague.com" target="_blank">Premier League</a> official website covering ongoing maintenance and some feature projects. Soon after I took the frontend development lead for space of 3 months.

Former projects:

- <a href="https://www.premierleague.com/stats/player-comparison" target="_blank">Premier League: Player Comparison</a>
- <a href="https://www.premierleague.com/history/dashboard" target="_blank">Premier League: History</a>

I then moved to lead the World Rugby frontend development team and what would become my long term Pulselive client. From spring 2018 my team and I managed and/or produced 10 different websites, spread into 15 different codebases.

World Rugby Sites:

- <a href="https://www.world.rugby" target="_blank">World Rugby</a>
- <a href="https://www.rugbyworldcup.com" target="_blank">Rugby World Cup</a>
- <a href="https://www.rugbyworldcup.com/2019" target="_blank">Rugby World Cup 2019</a>
- <a href="https://www.rugbyworldcup.com/2021" target="_blank">Rugby World Cup 2021</a>
- <a href="https://www.rugbyworldcup.com/2023" target="_blank">Rugby World Cup 2023</a>
- <a href="https://www.world.rugby/sevens-series" target="_blank">HSBC Sevens Series</a>
- <a href="https://www.world.rugby/halloffame" target="_blank">Hall of Fame</a>
- <a href="https://www.world.rugby/media-zone" target="_blank">Media Zone</a>
- <a href="https://www.women.rugby" target="_blank">Women In Rugby</a>
<!-- + <a href="https://www.pulselive.com" target="_blank">Keep Rugby Clean</a> -->

While working for World Rugby I had the opportunity to deliver very interesting and complex digital products, such as:

- a SSO (Single Sign On) solution for the whole sites cluster;
- architecting the official Match Predictor game using a mix of AWS services and frontend technologies, game used for both the HSBC Sevens Series and the Rugby World Cup 2019 tournaments;
- and the Media Zone content distribution website for the exclusive use of the press, to name a few.

Former projects:

- <a href="https://www.rugbyworldcup.com/2019/predictor" target="_blank">Rugby World Cup 2019 Predictor</a>
- <a href="https://www.rugbyworldcup.com/2019/match/final" target="_blank">Rugby World Cup 2019 Match Centre</a>
- <a href="https://www.rugbyworldcup.com/2019/greatestxv" target="_blank">Rugby World Cup 2019 Greatest XV</a>
- <a href="https://www.world.rugby/sevens-series/predictor/about" target="_blank">Sevens Series Predictor</a>
- <a href="https://www.world.rugby/media-zone" target="_blank">Media Zone</a>

During the first quarter of 2020 I had the chance to lead the ongoing maintenance of the Everton FC official website.

Former clients:

- <a href="https://www.evertonfc.com" target="_blank">Everton FC</a>

## Design Culture — 01/2017 – 06/2017

#### Roles:

_Frontend Developer_

<a href="https://www.design-culture.co.uk" target="_blank">Design Culture</a>‘s clients are primarily within the charity and healthcare sector. I’ve mainly helped in restructuring their legacy projects, cleaning-up and reorganising their repositories while giving maintenance to their clients sites and developing new projects.

Former projects:

- <a href="https://www.thepayrollgivingteam.co.uk" target="_blank">The Payroll Giving Team</a>
- <a href="https://www.rambertschool.org.uk" target="_blank">Rambert School</a>
- <a href="https://thecharterhouse.org" target="_blank">The Charterhouse</a>

## Exposure Digital — 03/2016 – 09/2016

#### Roles:

_Web Developer_

<a href="https://europe.exposure.net" target="_blank">Exposure</a> is an independent communications agency founded in 1993.  
<a href="https://exposed.net" target="_blank">Exposure Digital</a> is a digital centric creative agency.

During my time in Exposure I mainly provided maintenance and service for projects such as <a href="https://findtheone.triumph.com" target="_blank">Triumph´s Find The One</a>, the Merlin Group’s <a href="https://www.visitsealife.com" target="_blank">Sealife</a>, <a href="https://www.thorpepark.com" target="_blank">Thorpe Park</a> & <a href="https://www.altontowers.com" target="_blank">Alton Towers</a>, as well as building sites such as <a href="https://www.tui.co.uk" target="_blank">Thomson Airways TUI</a> and Radio Kangol.

Former projects:

- <a href="https://www.tui.co.uk" target="_blank">Thomson Airways TUI</a> (UK)
- Radio Kangol (US)

## Prompt Dev & 81Agency — from 2015

#### Prompt Dev roles:

_Full Stack Developer_

Prompt Dev is the sole trading name I’ve been using for my European customers in the UK, France and Spain.

Former clients:

- <a href="http://mlopticien.fr" target="_blank">ML Opticien</a> (FR)
- <a href="https://www.texeurop.com" target="_blank">Texeurop</a> (FR)
- Walford Wilkie (UK)

81Agency roles:  
_Lead Developer_

81Agency is a brand communication agency for the premium and prestige marketplace and its my first UK successful partnership.

Former clients:

- <a href="https://savvysand.com" target="_blank">Savvy+Sand</a> (UK)
- <a href="https://birchalltea.co.uk" target="_blank">Birchall Tea</a> (UK)

## Nrmal & Savvy Studio — 2008/2014

#### Nrmal: The Early Years (2008-2010) roles:

_Web Maintainer_  
_Blog Co-editor_  
_Podcast Producer_  
_Production Assistant (Events/Concerts)_

Nrmal started as an event promotion agency and a music blog. By that time, I was in charge of maintaining the site, co-editing the blog and assisting in events production. <a href="https://www.nrmal.net" target="_blank">Nrmal.net</a> first version was built using <a href="https://symfony.com" target="_blank">Symfony</a>.

#### Podcast Nrmal (2009-2011) roles:

_Podcast Producer & Co-director_

96 Episodes full of fun and great music. Collaborations range from <a href="http://www.matiasaguayo.com" target="_blank">Matias Aguayo</a> (Kompakt/Cómeme), Tim Sweeney (DFA, <a href="https://beatsinspace.net" target="_blank">Beats in Space</a>), <a href="https://kompakt.fm/artists/rebolledo" target="_blank">Rebolledo</a> (Kompakt, Cómeme), <a href="https://foolsgoldrecs.com/artists/sammy-bananas" target="_blank">Sammy Bananas</a> (Fools Gold), <a href="https://yelle.fr" target="_blank">Yelle</a> (Source Etc), local legends such as Toy Selectah (Mad Decent) and many more.

Producing Podcast Nrmal was a great and enjoyable experience, every month we would have different themes and amazing collaborations.

#### Nrmal Studio (2008-2010) roles:

_Web Developer/Webmaster_

A party of young proactive creatives and designers was soon established. Clients came pouring and the team began shaping into a branding studio.

I formally started working as a Web Developer & Webmaster.

#### Savvy Studio (2010-2014) roles:

_Head of Web Development_

As Head of Web Development more than 50 sites were built ranging from small to medium sized companies in a wide variety of disciplines, reaching out to international clients. Since then I’ve been working hard improving my development techniques.

Former clients:

- <a href="https://www.shaunfordandco.com" target="_blank">Shaun Ford and Co.</a> (CA)
- <a href="https://rgmx.mx" target="_blank">Rivero González</a> (MX)
- <a href="https://casabosques.co" target="_blank">Casa Bosques</a> (MX)

#### Festival Nrmal (2010-2014) roles:

_(2010-2013) Website Development_  
_(2010-2014) Production Assistant & Stage Manager_

During the festival years I built the sites for the 2010-2013 editions and worked closely in the festival’s promotion campaigns as well as assisting in the festival production up until the 2014 edition.

#### Norte Sonoro (2011-2012) roles:

_(2011 & 2012) Website Development_  
_(2011) Production Assistant & Stage Manager_

Norte Sonoro was a series of residencies that sought —through experimentation— to explore the sounds of northern Mexico, and one of Nrmal’s most ambitious projects.

For both editions an EP was released.
<a href="https://nortesonoro.bandcamp.com/album/norte-sonoro-ep-1" target="_blank">Norte Sonoro EP 1</a> held in Monterrey, curated and produced by Toy Selectah (Mad Decent), and <a href="https://nortesonoro.bandcamp.com/album/norte-sonoro-ep-2-2" target="_blank">Norte Sonoro EP 2</a> held in Tijuana, curated by <a href="https://www.negrophonic.com/2013/norte-sonoro-tijuana/" target="_blank">DJ/Rupture</a> and produced by <a href="https://soundcloud.com/losmacuanos" target="_blank">Los Macuanos</a>.
