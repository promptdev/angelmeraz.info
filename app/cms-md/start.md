Hi, I'm <a href="https://www.linkedin.com/in/angelmeraz/" target="_blank" className="text-accent">Angel Meraz <i className="fa-brands fa-linkedin text-base"> </i></a>.  
Welcome to my contact page.

I'm a Full Stack Javascript Engineer passionate about technology and design with 10+ years experience developing Web Applications and User Interfaces. I have a strong appetite for new opportunities, interested in making collaborations exciting.

<i className="fas fa-briefcase text-accent" aria-hidden="true"></i>
Currently open for a new role. <a href="mailto:hola@angelmeraz.info">Get in touch!</a>

<i className="fa-brands fa-aws text-accent w-[18px] text-base"></i>
<a href="https://www.credly.com/badges/b4ced796-78f2-4514-a55d-8eb81030ed8d/public_url" target="_blank">Certified AWS Cloud Practitioner</a>
