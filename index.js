// Keystone main instance
const { Keystone } = require('@keystonejs/keystone');
const initialiseData = require('./config/InitialData');

// Main dependencies
const { AdminUIApp } = require('@keystonejs/app-admin-ui');
const { GraphQLApp } = require('@keystonejs/app-graphql');
const { NextApp } = require('@keystonejs/app-next');

// Project Config
const CONFIG = require('./config');

// MongoDB Setup
const { MongooseAdapter: Adapter } = require('@keystonejs/adapter-mongoose');
const adapterConfig = {
  mongoUri: CONFIG.DATABASE_URI,
};

// Keystone main instance
const keystone = new Keystone({
  adapter: new Adapter(adapterConfig),
  onConnect: process.env.CREATE_TABLES !== 'true' && initialiseData,
  secureCookies: false,
  cookieSecret: process.env.COOKIE_SECRET,
});

// Schemas to Lists
const Page = require('./lists/Page');
keystone.createList('Page', Page);

const Project = require('./lists/Project');
keystone.createList('Project', Project);

const BlogPost = require('./lists/BlogPost');
keystone.createList('BlogPost', BlogPost);

const User = require('./lists/User');
keystone.createList('User', User);

// const MediaFile = require('./lists/MediaFile')
// keystone.createList('MediaFile', MediaFile)

// Authentication
const { PasswordAuthStrategy } = require('@keystonejs/auth-password');
const authStrategy = keystone.createAuthStrategy({
  type: PasswordAuthStrategy,
  list: 'User',
});

module.exports = {
  keystone,
  apps: [
    new GraphQLApp(),
    new AdminUIApp({
      name: CONFIG.PROJECT_NAME,
      enableDefaultRoute: false,
      authStrategy,
    }),
    new NextApp({
      dir: 'app',
    }),
  ],
  configureExpress: (app) => {
    app.set('trust proxy', true);
  },
};
