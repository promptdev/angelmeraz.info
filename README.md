# AngelMeraz.info

Basic KeystoneJS + MongoDB + NextJS boilerplate stater with Page and User (with basic authentication) lists. Both manageable through the admin application at `localhost:3000/admin`.

Work in progress...

## Dependencies

```
$ node --version
v16.16.0
$ npm --version
8.11.0
$ mongo --version
MongoDB shell version v4.4.3
```

Is recommended to use `mongodb-version-manager` to manage different MongoDB versions in your local environment. Paired with this is recommended to use `mongodb-runner` to run local MongoDB instances.

## Running the Project.

If using `mongodb-version-manager` & `mongodb-runner` run:

```
$ pnpm add -g mongodb-version-manager mongodb-runner
$ m install 4.4.3
# or, if already installed
$ m use 4.4.3
$ mongodb-runner start
```

Ensure the right MongoDB version is running with the following commands:

```
$ mongo --version
MongoDB shell version v4.4.3
$ mongos --version
mongos version v4.4.3
$ mongod --version
db version v4.4.3
```

If running MongoDB differently ensure is already started before running the following commands:

```
$ pnpm
$ pnpm dev
```

Once running, the Keystone Admin UI is reachable via `localhost:3000/admin`.

The very first time the Keystone instance runs and connects to the MongoDB it will try to create the first using by running `./config/initialData.js`. If successful it will output the users credentials on the CLI.

### Troubleshooting Admin UI access

If you have problems logging into the Keystone Admin UI follow this steps:

1. First, disable authentication on the Admin UI by removing authStrategy from the new AdminUIApp() call:

```
- const admin = new AdminUIApp({ authStrategy });
+ const admin = new AdminUIApp();
```

2. Second, disable access control by removing access from the `keystone.createList('User', ... )` call:

```
-  access: {
-    read: access.userIsAdminOrOwner,
-    update: access.userIsAdminOrOwner,
-    create: access.userIsAdmin,
-    delete: access.userIsAdmin
-    auth: true,
-  },
```

Restart your Keystone App, and visit http://localhost:3000/admin/users - you should now be able to access the Admin UI without logging in.

3. Create a User (be sure to set both a username and password).

4. Add the `authStrategy` config back to the `new AdminUIApp()` call, effectively reverting steps 1 and 2.

#### Localhost account credentials

**User:** hola@angelmeraz.info  
**Password:** Facil12!

#### Production Keystone Admin URL

https://angelmeraz-info.herokuapp.com/admin

## Config

There are 2 main config files:

1. Server-side `./config/index.js`

   - `DATABASE_URI`
     The database address:local/production

   - `PROJECT_NAME`
     The project name

2. Client-side `./app/config/index.js`

   - `DOMAIN`
     The domain. Local should be set to `http://localhost:3000`

   - `SLUGS`
     Application reserved slugs. Comes with a `HOMEPAGE` constant. Add here other reserved slugs.
